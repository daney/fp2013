module Sudoku where

import Test.QuickCheck
import Data.Char
import Data.List
import Data.Maybe
-------------------------------------------------------------------------

data Sudoku = Sudoku { rows :: [[Maybe Int]] }
 deriving ( Show, Eq )

-- allBlankSudoku is a sudoku with just blanks
allBlankSudoku :: Sudoku
allBlankSudoku = Sudoku $ replicate 9 ( replicate 9 Nothing)

-- isSudoku sud checks if sud is really a valid representation of a sudoku
-- puzzle
isSudoku :: Sudoku -> Bool
isSudoku s = length (rows s) == 9 && and [ length row == 9| row <- (rows s)] 
                && validDigit [r | r<-rows s ]
  where   validDigit []       = True
          validDigit (xx:xss) = all (<>) (map (fromJust') xx) && validDigit xss
          (<>) b = b>0 && b<10                 
          fromJust' Nothing   = 1
          fromJust' (Just d)  = d 

-- isSolved sud checks if sud is already solved, i.e. there are no blanks
isSolved :: Sudoku -> Bool
isSolved s = isSudoku s && notElem Nothing (concat (rows s))

-------------------------------------------------------------------------

-- printSudoku sud prints a representation of the sudoku sud on the screen
printSudoku :: Sudoku -> IO ()
printSudoku sud = do putStr (unlines [ rowToString row | row <- (rows sud)])
    where rowToString row = foldl (++) "" $ map cellToChar row
          cellToChar Nothing  = "."
          cellToChar (Just a) = show a

-- readSudoku file reads from the file, and either delivers it, or stops
-- if the file did not contain a sudoku 
readSudoku :: FilePath -> IO Sudoku
readSudoku fileP = do
                 s <- readFile fileP     
                 m <- return (linesToRows $ lines s)
                 return $ validSudoku  $ Sudoku m 
  where linesToRows  []    = []
        linesToRows (x:xs) = charToCell x : linesToRows xs
        charToCell ""                 = []
        charToCell (c:cs) | c == '.'  = Nothing : charToCell cs 
                          | isDigit c = Just (digitToInt c) : charToCell cs 
                          | otherwise = error "Not a Sudoku" 
        validSudoku sud | isSudoku  sud = sud   
                        | otherwise     = error "Not a Sudoku" 
-------------------------------------------------------------------------

-- cell generates an arbitrary cell in a Sudoku
cell :: Gen (Maybe Int)
cell = frequency
  [(90, return Nothing),
   (9, do r <- choose(1,9)
          return (Just r))]

-- an instance for generating Arbitrary Sudokus
instance Arbitrary Sudoku where
  arbitrary =
    do rows <- sequence [ sequence [ cell | j <- [1..9] ] | i <- [1..9] ]
       return (Sudoku rows)

prop_Sudoku :: Sudoku -> Bool
prop_Sudoku s = isSudoku s 
-------------------------------------------------------------------------

type Block = [Maybe Int]

isOkayBlock :: Block -> Bool
isOkayBlock block = length noBlanks == length (nub noBlanks)
  where  noBlanks = filter (/= Nothing) block

blocks :: Sudoku -> [Block]
blocks sud = rows sud ++ cols sud ++ blocks3By3 sud
 where   
  cols s  = transpose $ rows sud
  blocks3By3 s = (joinRows (take 3) [r |r<-rows s]) 
                 ++ (joinRows (take 3) $ map (drop 3) [r |r<-rows s]) 
                 ++ (joinRows (drop 6) [r |r<-rows s])
  joinRows _ []             = []
  joinRows  f (x:y:z:xyzs)  = (concat $ map (f) [x,y,z] ): joinRows (f) xyzs
 
isOkay :: Sudoku -> Bool
isOkay = all isOkayBlock . blocks

--------------------------3B--------------------------------------------
type Pos = (Int,Int)

blanks :: Sudoku -> [Pos]
blanks s | isSudoku s = eachRow 0 (rows s)
         | otherwise  = error "not a Sudoku" 
  where
   eachRow n rs | n > 8     = [] 
                | otherwise = cellsInRow n 0 (rs !! n) ++  eachRow (n+1) rs
   cellsInRow n m s | m > 8                = []
   cellsInRow n m s | (s !! m ) == Nothing = [(n,m)] ++ cellsInRow n (m+1) s
                    | otherwise            = cellsInRow n (m+1) s

-- getCell returns a cell value by its position
getCell :: Sudoku -> Pos -> Maybe Int
getCell s (x,y) = ((rows s) !! x) !! y

prop_BlankCells :: Sudoku -> Bool
prop_BlankCells  sud = isBlank sud (blanks sud)
 where  
  isBlank s []         = True
  isBlank s ((r,c):ps) = ((rows s) !! r) !! c == Nothing && isBlank s ps

(!!=) :: [a] -> (Int,a) -> [a]
(!!=) list (pos,value) | isValidPos = (take pos list) ++ [value] ++ (drop (pos + 1) list)
                       | otherwise = error "Index out of bounds!"
   where isValidPos = pos >= 0 && pos < length list

prop_IndexValue :: (Eq a, Num a) => [a] -> (Int,a) -> Bool
prop_IndexValue [] _ = True
prop_IndexValue list (pos, value) = 
    newList !! pos' == value &&
    length (newList \\ list) <= 1 && -- At most one element may differ.
    length list == length (list !!= (pos', value))
  where pos' = abs $ pos `rem` length list
        newList = list !!= (pos', value)


capIndex index limit = abs $ index `rem` limit

update :: Sudoku -> Pos -> Maybe Int -> Sudoku
update sud (row, col) value 
    | isValidPos (row, col) = Sudoku rows'
    | otherwise             = error "Index out of bounds!"
  where 
        theRows = rows sud
        rows' = (take row theRows) ++ 
                [(theRows !! row) !!= (col, value)] ++
                (drop (row+1) theRows)

isValidPos :: Pos -> Bool        
isValidPos (row,col) = row `elem` [0..8] && col `elem` [0..8]

prop_update :: Sudoku -> Pos -> Maybe Int -> Bool
prop_update sud (row, col) value = getCell sud' (row',col') == value'
  where 
        sud' = update sud (row',col') value'
        row' = capIndex row 9
        col' = capIndex col 9
        value' | value == Nothing = value
               | otherwise        = Just $ capIndex (fromJust value) 9 + 1

candidates :: Sudoku -> Pos -> [Int]
candidates  sud pos | not $ isValidPos pos       = error "Index out of bounds"
                    | getCell sud pos /= Nothing = []
                    | otherwise                  = candidates' 1 sud pos 
  where
   candidates' n sud (row,col)| n > 9     = [] 
                              | and $ map (notElem (Just n)) [blocks sud !! row, 
                                         blocks sud !! (col + 9), 
                                         blocks sud !! (blockNum (row,col) + 18)]
                                          = n : candidates' (n+1) sud (row,col)
                              | otherwise = candidates' (n+1) sud (row,col)   
  
   -- blockNum returns the block number(3 by 3) of a given position (r,c) 
   -- i.e. (0,1),(0,1),(0,2),(1,0)..(2,2) belong to first block 0 and etc.  
   blockNum (r,c) | r < 3     = if c < 3 then 0 else if c<6 then 3 else 6 
                  | r < 6     = if c < 3 then 1 else if c<6 then 4 else 7 
                  | otherwise = if c < 3 then 2 else if c<6 then 5 else 8
      
prop_Candidates :: Sudoku -> Property
prop_Candidates s = isOkay s ==> isSudoku s && eachCandidates s (blanks s)
  where 
   eachCandidates sud []     = True
   eachCandidates sud (b:bs) = isFineUpdate sud b (candidates sud b) 
                              && eachCandidates sud bs
   isFineUpdate sud p []       = True
   isFineUpdate sud p (v:vs)   = isOkay (update sud p (Just v)) 
                                 && isFineUpdate sud p vs  

------------------------------------------------------------------------
solve :: Sudoku -> Maybe Sudoku
solve sud | not $ isOkay sud && isSudoku sud = Nothing
          | otherwise = solve' sud
    where
          solve' sud | blanks sud == [] = Just sud
          solve' sud  = 
            let candidateSudokus = map (\candidate -> update sud cell (Just candidate)) 
                                       (candidates sud cell)
                cell = head $ blanks sud
            in listToMaybe $ catMaybes $ map solve' candidateSudokus

readAndSolve :: FilePath -> IO ()
readAndSolve filePath = do
                          sud <- readSudoku filePath
                          let solved = solve sud
                          if solved == Nothing then
                            putStrLn "(no solution)"
                          else
                            printSudoku $ fromMaybe allBlankSudoku solved

isSolutionOf :: Sudoku -> Sudoku -> Bool
isSolutionOf solution problem = isSolved solution && 
                                isOkay solution && 
                                digitsMaintained solution problem
    where 
      digitsMaintained sol prob = 
          and [getCell sol (row,col) == getCell prob (row,col) | 
                row<-[0..8], col<-[0..8], (row,col) `notElem` (blanks prob)] 

prop_SolveSound :: Sudoku -> Property
prop_SolveSound sud = hasSolution ==> isSolutionOf solution sud
    where
      maybeSolved = solve sud
      solution = fromMaybe allBlankSudoku maybeSolved
      hasSolution = maybeSolved /= Nothing
